"""Determine whether one envelope can be nested ino other. The program must input
floating point entries. The program asks custom envelope sizes one parameter
at a time. After each counting program asks the user if he wants to continue.
If the user answers “y” or “yes”  the program continues to work one more time."""


from abc import ABC, abstractmethod
from typing import Any


class Input(ABC):

    @abstractmethod
    def inputted_data(self) -> Any:
        pass

class IntInput(Input):

    def __init__(self, value: str):
        self._value = value

    def inputted_data(self) -> int:
        while True:
            try:
                return int(input(f"{self._value} \n"))
            except ValueError:
                print("Incorrect data, input must be integer")


class Requirements(ABC):

    @abstractmethod
    def passed(self, value: Any) -> bool:
        pass


class GreaterThanZero(Requirements):

    def passed(self, value: int) -> bool:
        return value > 0


class CheckRequirements(Requirements):
    def __init__(self, *requirements: Any):
        self._test = requirements

    def passed(self, value: int) -> int:
        for requirement in self._test:
            if not requirement.passed(value):
                print(f"{requirement.__class__.__name__} is failed.\n"
                      "Try to run program again!")
                quit()
            return value


class Envelope:

    def __init__(self, side_1: float, side_2: float):
        self._side_1 = side_1
        self._side_2 = side_2

    def __lt__(self, other) -> bool:
        return self._side_1 < other._side_1 and self._side_2 < other._side_2


class NewExecution:

    def choose_action(self) -> bool:
        enter_choice = input("Do you want to continue, enter y or yes: ")
        if enter_choice.lower() in ("yes", "y"):
            return True
        return False


if __name__ == '__main__':
    while True:
        length = IntInput("Please, enter length")
        height = IntInput("Please, enter height")
        checked_side = CheckRequirements(GreaterThanZero())
        first_envelope = Envelope(
                                  checked_side.passed(length.inputted_data()),
                                  checked_side.passed(height.inputted_data())
            )
        second_envelope = Envelope(
                                  checked_side.passed(length.inputted_data()),
                                  checked_side.passed(height.inputted_data())
            )
        print(
            f"Second envelope can be nested it's - {first_envelope > second_envelope}\n"
            f"First envelope can be nested it's - {second_envelope > first_envelope}"
        )
        if not NewExecution().choose_action():
            break



