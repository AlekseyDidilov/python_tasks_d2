"""Create a console program that performs the output of triangles in descending order.
their area. After adding each new triangle, the program asks if user add one more.
If the user answers “y” or “yes” (case insensitive),the program will ask you to enter
data for another triangle, otherwise it displays result in the console.
- The calculation of the area of a triangle should be set using the Heron formula.
- Input format (comma separated): <Name>, <side_1>, <side_2>, <side_3>.
- An application must handle floating point numbers."""

from typing import List


def creation_triangles(name: str, side_a: float, side_b: float, side_c: float) -> List:
    perimeter = (side_a + side_b + side_c)/2
    square = round(((perimeter * (perimeter - side_a) *
                                 (perimeter - side_b) *
                                 (perimeter - side_c)) ** 0.5), 2)
    triangle_name_square = list(name)
    triangle_name_square.append(square)
    return triangle_name_square


def str_input() -> str:
    while True:
        get_data = input("Please, enter triangle name and size of sides \n"
                         "Input format - 'name, side_a, side_b, side_c':\n")

        triangle_data = get_data.split(",")
        if len(triangle_data) == 4 and is_valid_sides((triangle_data[1]),
                                                      (triangle_data[2]),
                                                      (triangle_data[3])):
            return triangle_data


def is_valid_sides(side_a: str, side_b: str, side_c: str) -> bool:
    try:
        float(side_a)
        float(side_b)
        float(side_c)
    except ValueError:
        print("Sides must be in float type")
        return False
    if (float(side_a) + float(side_b) > float(side_c) and
        float(side_a) + float(side_c) > float(side_b) and
        float(side_b) + float(side_c) > float(side_a)) and \
            min(float(side_a), float(side_b), float(side_c)) > 0:
        return True
    return False



def choose_action() -> bool:
    enter_choice = input("Do you want to continue, enter y or yes:\n")
    if enter_choice.lower() in ("yes", "y"):
        return True
    else:
        return False


def print_triangles(triangles: List) -> None:
    print("=====Triangle's list:=====")
    sort_triangles = sorted(triangles, reverse=True)
    for triangle, key in enumerate(sort_triangles, 1):
        print(triangle, key, end="\n")


def main():
    is_continue = True
    triangles_list = list()
    while is_continue:
        input_value = str_input()
        new_triangle = creation_triangles(str(input_value[0]),
                                          float(input_value[1]),
                                          float(input_value[2]),
                                          float(input_value[3]))
        triangles_list.append(new_triangle)
        is_continue = choose_action()
    print_triangles(triangles_list)


if __name__ == '__main__':
    main()
